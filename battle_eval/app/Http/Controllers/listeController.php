<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class listeController extends Controller
{
    public function users () 
    {
        $user = DB::table('users')->orderBy('users.id')->get();
        $user2 = json_decode($user);
        return view('utilisateurs', ['users' => $user2]);
    }

    public function evenement () 
    {
        $event = DB::table('events')->orderBy('events.id')->get();
        $event2 = json_decode($event);
        return view('evenement', ['events' => $event2]);
    }
}
