<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Topic;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        
        $topic = new Topic;
        $topic->title = $request->input('titleTopic');
        $topic->text = $request->input('textTopic');
        $topic->save();
        return view('/sent');
    }

    public function home() 
    {
        return view('home');
    }
}

