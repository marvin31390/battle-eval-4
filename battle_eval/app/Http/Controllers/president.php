<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Event;
class president extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function president (Request $request) 
    {
        if($user = Auth::user()->is_admin != 1)
    {
        return view('home');
    }
    else 
    {
        $event = new event;
        $event->location = $request->input('locationEvent');
        $event->date = $request->input('dateEvent');
        $event->save();
        return view('/sent');
    }
    }

    public function event ()
    {
        if($user = Auth::user()->is_admin != 1)
    {
            return view('home');
    }
        return view('event');
    }
    
}
