<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

Route::get('/', 'HomeController@home')->name('home');

Route::get('/home', 'HomeController@home');

Route::get('/sent', 'HomeController@index');

Route::get('/roulette', 'rouletteController@roulette')->name('roulette');

Route::get('/roulette/{id}', 'rouletteController@update')->name('update');

Route::get('/noMoreTopic', 'HomeController@home');

Route::get('/event', 'president@event')->name('event');

Route::get('/sentPresident', 'president@president');

Route::get('/utilisateurs', 'listeController@users')->name('utilisateurs');

Route::get('/evenement', 'listeController@evenement')->name('evenement');



