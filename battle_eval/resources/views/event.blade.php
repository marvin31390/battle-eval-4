@extends('layouts.app')
@section('title', 'Event')
@section('content')
<div class="container-fluid">
<form method='GET' action="/sentPresident">
  <div class="form-group">
    <input type="text" class="form-control" name='locationEvent' placeholder="lieu de votre événement">
  </div>
  <div class="form-group">
    <input type="date" class="form-control" name='dateEvent' min="{{ date('Y-m-d') }}">
  </div>
  <button type="submit" class="btn btn-primary mb-2">Envoyé votre sujet</button>
</form>
</div>
@endsection