@extends('layouts.app')
@section('title', 'Battle eval')
@section('content')
<div class="container-fluid">
<?php $user = auth()->user();?>
@if ($user->is_admin == 1)
<div class="container-fluid">
    <a class="btn btn-primary bouton mb-3" href="/event">créer un événement ?</a>
  </div>
@endif
<form method='GET' action="/sent">
  <h2>Envoyez nous vos sujet pour la battle de jeudi prochain !!! </h2>
  <div class="form-group">
    <input type="text" class="form-control" name='titleTopic' placeholder="titre de votre sujet">
  </div>
  <div class="form-group">
    <textarea class="form-control" name="textTopic" placeholder='texte de votre sujet' rows="3"></textarea>
  </div>
  <button type="submit" class="btn btn-primary mb-2">Envoyé votre sujet</button>
</form>
</div>
@endsection
