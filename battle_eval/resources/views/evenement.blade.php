@extends('layouts.app')
@section('title', 'Liste des evenements')
@section('content')
<div class="container-fluid">
    @foreach($events as $event)
    <?php
        $dateDb = $event->date;
        setlocale(LC_TIME, 'fr_FR.utf8','fra');

        $dateFr = strftime("%A %d %B %G", strtotime($dateDb));
    ?>
    <h2>{{$event->location}} le {{$dateFr}}</h2>
    @endforeach
</div>
@endsection