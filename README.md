# battle eval 4

## Installation



```bash
npm install
```

## Usage

```bash
php artisan serve
npm run dev
php artisan db:create 
php artisan migrate


```

## Contributing
Massot Marvin

## License
No.

## lien vers les diagrammes : 

MCD : https://lucid.app/lucidchart/invitations/accept/2d02a9ed-7b3f-4b3f-a136-cc9a4a9235b0

diagramme séquentiel : https://lucid.app/lucidchart/invitations/accept/6f8b6144-778f-4271-90bb-af0a9f7f1c0b

diagramme de cas d'utilisation : https://lucid.app/lucidchart/invitations/accept/c86fc263-1ee0-4e3b-8262-2da2c71308bf

vous trouverez les diagrammes dans battleeval4/diagramme

## version en ligne


http://battlemarvin.herokuapp.com/


## asana


https://app.asana.com/0/1199967421733957/board


## autre version du projet

https://gitlab.com/marvin31390/battle-eval
https://gitlab.com/marvin31390/battle-eval2
https://gitlab.com/marvin31390/battle-eval-3
